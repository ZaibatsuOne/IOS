//
//  WebViewTests.swift
//  TestIosTests
//
//  Created by Антон Темеров on 25.06.2024.
//

import XCTest
import WebKit
@testable import TestIos

class WebViewControllerTests: XCTestCase {
    
    func testInitialization() {
        let url = URL(string: "https://www.apple.com")!
        let webViewController = WebViewController(url: url)
        
        XCTAssertNotNil(webViewController.view)
        XCTAssertEqual(webViewController.url, url)
    }
    
    func testViewDidLoadSetsUpUI() {
        let url = URL(string: "https://www.apple.com")!
        let webViewController = WebViewController(url: url)
        _ = webViewController.view
        
        XCTAssertEqual(webViewController.titleLabel.text, "Web View")
        XCTAssertEqual(webViewController.headerView.backgroundColor, UIColor.lightGray.withAlphaComponent(0.3))
    }
    
    func testWebViewLoadRequest() {
        let url = URL(string: "https://www.apple.com/")!
        let webViewController = WebViewController(url: url)
        _ = webViewController.view
        
        let loadedURL = webViewController.webView.url?.absoluteString.trimmingCharacters(in: CharacterSet(charactersIn: "/"))
        let expectedURL = url.absoluteString.trimmingCharacters(in: CharacterSet(charactersIn: "/"))
        
        XCTAssertEqual(loadedURL, expectedURL)
    }
    
}
