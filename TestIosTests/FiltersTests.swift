//
//  FiltersTests.swift
//  TestIosTests
//
//  Created by Антон Темеров on 25.06.2024.
//

import XCTest
@testable import TestIos

class FiltersTests: XCTestCase {

    var sut: FilterViewController!

    override func setUp() {
        super.setUp()
        sut = FilterViewController()
        sut.loadViewIfNeeded()
    }

    override func tearDown() {
        sut = nil
        super.tearDown()
    }

    func testInitialFilterValues() {
        XCTAssertEqual(sut.selectedContentTypeSubject.value, .image)
        XCTAssertEqual(sut.selectedSizeSubject.value, .any)
        XCTAssertEqual(sut.selectedCountrySubject.value, .ru)
        XCTAssertEqual(sut.selectedLanguageSubject.value, .english)
    }

    func testApplyFilters() {
        let delegateMock = FilterViewControllerDelegateMock()
        sut.delegate = delegateMock

        sut.applyFilters()

        XCTAssertTrue(delegateMock.didApplyFiltersCalled)

        guard let appliedFilters = delegateMock.appliedFilters else {
            XCTFail("Applied filters should not be nil")
            return
        }

        // Compare individual key-value pairs in dictionaries
        XCTAssertEqual(appliedFilters["type"] as? String, sut.filters["type"] as? String)
        XCTAssertEqual(appliedFilters["size"] as? String, sut.filters["size"] as? String)
        XCTAssertEqual(appliedFilters["country"] as? String, sut.filters["country"] as? String)
        XCTAssertEqual(appliedFilters["language"] as? String, sut.filters["language"] as? String)
    }


    // Example mock delegate class for testing purposes
    class FilterViewControllerDelegateMock: FilterViewControllerDelegate {
        var didApplyFiltersCalled = false
        var appliedFilters: [String: Any]?

        func filterViewController(_ viewController: FilterViewController, didApplyFilters filters: [String : Any]) {
            didApplyFiltersCalled = true
            appliedFilters = filters
        }
    }

}
