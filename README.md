# Image Search App
Min deploy target - 14 iOS

## TASK

You need to create an app that will search images on Google by the query text. The API that must return the result is described here: https://serpapi.com/images-results
The page must look like the page of the NavigationController where a search field must be placed on the top of the page where users can write the text for the query. The result images must be shown below the search field as a collection view.
A tap on the preview image must open an image in the full screen view with buttons “Next” and “Prev” that allow users to view previous and next images from the search result. Also, please add a button to open the original source page. Clicking on that button must open a website on Web View Controller inside the app. Users can close that Web View to return to images.
App must be made using UIKit.
API service also allows pagination for loading images. Please implement pagination on scroll
(page size is 100 in SerpApi, and can’t be changed).
Also, the service allows to apply different filters for results, we could add “Tools” button where
we could select for example a type of responses (let’s keep images by default and video as an
additional type of supported media), the user could filter by the result’s size (any size (default),
large, medium, icon), filter results by country, language (multiple languages).
It will be good if the images are cached on a device.

## Completed tasks
Main UI:
- Add UISearchBar. ✅
- Create a CollectionView. ✅

API integration:
- Request to SerpApi. ✅
- Data display. ✅
- Scroll pagination. ✅

Full screen image:
- Opening an image. ✅
- Next/Prev buttons ✅
- Source button. ✅

Web View Controller:
- Close button. ✅

Filters:
- “Tools” button. ✅
- Filters by type. ✅
- Filters by size. ✅
- Filters by country. ✅
- Filters by language. ✅

Image caching:
- Implementation of caching. ✅

**Note**
Multiple language selection: ❌ (API does not allow multiple language selection for images and videos)

## Technologies Used

- **Network**
  - `Moya`: A network abstraction layer on top of Alamofire.
  - `Moya/Combine`: Combine framework support for Moya.

- **UI**
  - `SnapKit`: A DSL to make Auto Layout easy on iOS.
  - `Kingfisher`: A powerful, pure-Swift library for downloading and caching images from the web.

## Installation

To set up the project, follow these steps:

1. **Clone the Repository**:
   ```sh
   git clone https://gitlab.com/ZaibatsuOne/IOS.git
   cd ImageSearchApp

2. **Install CocoaPods Dependencies:**
   ```sh
   sudo gem install cocoapods

   pod install

3. **Open the Project:**
   ```sh
   open ImageSearchApp.xcworkspace


## SCREENS
<p align="center">
  <img src="assets/emptyMain.jpg" alt="Screenshot 1" width="200"/>
  <img src="assets/main.jpg" alt="Screenshot 2" width="200"/>
  <img src="assets/filters.jpg" alt="Screenshot 3" width="200"/>
  <img src="assets/photo.jpg" alt="Screenshot 3" width="200"/>
</p>



