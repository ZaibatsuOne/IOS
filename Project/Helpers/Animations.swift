//
//  Animations.swift
//  TestIos
//
//  Created by Антон Темеров on 24.06.2024.
//

import UIKit

class Animations {
    
    static func press(view: UIView?) {
        guard let view = view else {
            return
        }
        
        UIView.animate(withDuration: 0.2, animations: {
            view.transform = CGAffineTransform(scaleX: 0.92, y: 0.92)
        }, completion: { _ in
            UIView.animate(withDuration: 0.2, animations: {
                view.transform = .identity
            })
        })
    }
    
}
