//
//  UserSettings.swift
//  TestIos
//
//  Created by Антон Темеров on 24.06.2024.
//

import Foundation

@objc class UserSettings: NSObject {
    
    static let defaults = UserDefaults.standard
    
    fileprivate enum Keys: String {
        case contentType
        case size
        case country
        case language   
    }
    
    static var contentType: String? {
        get {
            return defaults.string(forKey: Keys.contentType.rawValue)
        }
        set {
            defaults.set(newValue, forKey: Keys.contentType.rawValue)
        }
    }
    
    static var size: String? {
        get {
            return defaults.string(forKey: Keys.size.rawValue)
        }
        set {
            defaults.set(newValue, forKey: Keys.size.rawValue)
        }
    }
    
    static var country: String? {
        get {
            return defaults.string(forKey: Keys.country.rawValue)
        }
        set {
            defaults.set(newValue, forKey: Keys.country.rawValue)
        }
    }
    
    static var language: String? {
        get {
            return defaults.string(forKey: Keys.language.rawValue)
        }
        set {
            defaults.set(newValue, forKey: Keys.language.rawValue)
        }
    }
    
}
