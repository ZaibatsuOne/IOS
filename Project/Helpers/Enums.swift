//
//  Enums.swift
//  TestIos
//
//  Created by Антон Темеров on 24.06.2024.
//

// MARK: - FILTER VALUES
enum ContentType: String {
    case image = "images"
    case video = "video"
    
    var displayName: String {
        switch self {
        case .image: return "Image"
        case .video: return "Video"
        }
    }
}

enum ImageSize: String {
    case any = ""
    case large = "l"
    case medium = "m"
    case icon = "i"
    
    var displayName: String {
        switch self {
        case .any: return "Any"
        case .large: return "Large"
        case .medium: return "Medium"
        case .icon: return "Icon"
        }
    }
}

enum Language: String {
    case english = "en"
    case spanish = "es"
    case french = "fr"
    
    var displayName: String {
        switch self {
        case .english: return "English"
        case .spanish: return "Spanish"
        case .french: return "French"
        }
    }
}

enum Countries: String {
    case ru = "ru"
    case usa = "us"
    case uk = "uk"
    
    var displayName: String {
        switch self {
        case .ru: return "Russia"
        case .usa: return "USA"
        case .uk: return "United Kingdom"
        }
    }
}
