//
//  ImageLoader.swift
//  TestIos
//
//  Created by Антон Темеров on 24.06.2024.
//

import UIKit
import Kingfisher

class ImageLoader {
    
    static let shared = ImageLoader()
    private let imageCache = ImageCache.default
    static let retry = DelayRetryStrategy(maxRetryCount: 5, retryInterval: .seconds(3))

    @discardableResult
    static func setImage(url: URL, imgView: UIImageView) -> DownloadTask? {
        let options: KingfisherOptionsInfo = [
            .transition(.fade(0.2)),
            .cacheOriginalImage,
            .onFailureImage(UIImage(systemName: "xmark.icloud")),
            .retryStrategy(retry)
        ]
        
        imgView.kf.indicatorType = .activity
        
        return imgView.kf.setImage(
            with: url,
            options: options
        )
    }
}
