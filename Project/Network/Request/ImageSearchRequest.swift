//
//  ImageSearchRequest.swift
//  TestIos
//
//  Created by Антон Темеров on 24.06.2024.
//

struct ImageSearchRequest {
    var query: String?
    var imageSize: String?
    var contentType: String?
    var page: Int?
    var language: String?
    var country: String?
}

