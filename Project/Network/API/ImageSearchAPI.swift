//
//  ImageSearchAPI.swift
//  TestIos
//
//  Created by Антон Темеров on 24.06.2024.
//

import Moya

enum ImageSearchAPI {
    case searchImages(request: ImageSearchRequest)
}

extension ImageSearchAPI: TargetType {
    var baseURL: URL {
        return URL(string: "https://serpapi.com")!
    }

    var path: String {
        switch self {
        case .searchImages:
            return "/search"
        }
    }

    var method: Moya.Method {
        return .get
    }

    var task: Task {
        switch self {
        case .searchImages(let request):
            let contentType = request.contentType ?? ""
            let parameters = ImageSearchAPIHelper.shared.configureParameters(for: request, contentType: contentType)
            return .requestParameters(parameters: parameters, encoding: URLEncoding.queryString)
        }
    }

    var headers: [String : String]? {
        return ["Content-type": "application/json"]
    }
}
