//
//  ImageSearchResponse.swift
//  TestIos
//
//  Created by Антон Темеров on 24.06.2024.
//

import Foundation

struct ImageSearchResponse: Decodable {
    var imagesResults: [ImageResult]?
    var videoResults: [VideoResult]?
    var pagination: Pagination?
    var error: String?
    
    enum CodingKeys: String, CodingKey {
        case imagesResults = "images_results"
        case videoResults = "video_results"
        case pagination = "serpapi_pagination"
        case error
    }

    struct Pagination: Decodable {
        let current: Int
        let next: String
    }
}

struct ImageResult: Decodable {
    let position: Int
    let thumbnail: String
    let relatedContentId: String?
    let serpapiRelatedContentLink: String?
    let original: String
    let originalWidth: Int
    let originalHeight: Int
    let title: String?
    let tag: String?
    let link: String?
    let source: String?
    let isProduct: Bool?
    let inStock: Bool?
    
    enum CodingKeys: String, CodingKey {
        case position
        case thumbnail
        case relatedContentId = "related_content_id"
        case serpapiRelatedContentLink = "serpapi_related_content_link"
        case original
        case originalWidth = "original_width"
        case originalHeight = "original_height"
        case title
        case tag
        case link
        case source
        case isProduct = "is_product"
        case inStock = "in_stock"
    }
}

struct VideoResult: Decodable {
    let position: Int?
    let title: String?
    let link: String?
    let thumbnail: String?
    let date: String?
    let snippet: String?
    let videoLink: String?
    
    enum CodingKeys: String, CodingKey {
        case position
        case title
        case link
        case thumbnail
        case date
        case snippet
        case videoLink = "video_link"
    }
}

