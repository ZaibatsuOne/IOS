//
//  ImageSearchAPIHelper.swift
//  TestIos
//
//  Created by Антон Темеров on 25.06.2024.
//

import Foundation

class ImageSearchAPIHelper {
    static let shared = ImageSearchAPIHelper()
    
    private init() {}
    
    func configureParameters(for request: ImageSearchRequest, contentType: String) -> [String: Any] {
        var parameters: [String: Any] = [
            "q": request.query ?? "",
            "api_key": Globals.API_KEY
        ]
        
        switch contentType {
        case "images":
            configureImageSearchParameters(request: request, parameters: &parameters)
        case "video":
            configureVideoSearchParameters(request: request, parameters: &parameters)
        default:
            break
        }
        
        return parameters
    }
    
    private func configureImageSearchParameters(request: ImageSearchRequest, parameters: inout [String: Any]) {
        parameters["engine"] = "google_images"
        parameters["tbm"] = "isch"
        if let page = request.page {
            parameters["ijn"] = page
        }
        
        if let imageSize = request.imageSize {
            parameters["tbs"] = "isz:\(imageSize)"
        }
        
        if let language = request.language {
            parameters["hl"] = language
        }
        
        if let country = request.country {
            parameters["gl"] = country
        }
    }
    
    private func configureVideoSearchParameters(request: ImageSearchRequest, parameters: inout [String: Any]) {
        parameters["engine"] = "google_videos"
        parameters["tbm"] = "vid"
        if let page = request.page {
            parameters["start"] = page
        }
        
        if let language = request.language {
            parameters["hl"] = language
        }
        
        if let country = request.country {
            parameters["gl"] = country
        }
    }
}
