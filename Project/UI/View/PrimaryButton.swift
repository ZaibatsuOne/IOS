//
//  PrimaryButton.swift
//  TestIos
//
//  Created by Антон Темеров on 24.06.2024.
//

import UIKit

class PrimaryButton: UIButton {
    
    // MARK: - Initialization
    
    init() {
        super.init(frame: .zero)
        setupButton()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Private methods
    
    private func setupButton() {
        
        // Main state
        self.setTitleColor(.white, for: .normal)
        self.backgroundColor = Palette.Colors.primaryBlue
        self.layer.cornerRadius = 12
        self.contentEdgeInsets = UIEdgeInsets(top: 12, left: 16, bottom: 12, right: 16)
        
        // Shadow config
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.2
        self.layer.shadowOffset = CGSize(width: 0, height: 2)
        self.layer.shadowRadius = 2
        
        // Text config
        self.titleLabel?.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
        self.titleLabel?.adjustsFontSizeToFitWidth = true
        self.titleLabel?.minimumScaleFactor = 0.8
        
        self.addTarget(self, action: #selector(buttonPressed), for: .touchDown)
    }
    
    // MARK: - Actions
    
    @objc private func buttonPressed(_ sender: UIButton) {
        // Animate press
        Animations.press(view: sender)
        
        // Haptic Feedback
        let generator = UIImpactFeedbackGenerator(style: .light)
        generator.prepare()
        generator.impactOccurred()
    }
}
