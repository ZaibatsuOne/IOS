//
//  FullScreenImageView.swift
//  TestIos
//
//  Created by Антон Темеров on 24.06.2024.
//

import UIKit
import Combine

protocol FullscreenImageVCDelegate: AnyObject {
    func fullscreenImageVC(_ viewController: FullscreenImageVC, didSelectNextAt index: Int)
    func fullscreenImageVC(_ viewController: FullscreenImageVC, didSelectPreviousAt index: Int)
    func fullscreenImageVC(_ viewController: FullscreenImageVC, didTapOpenOriginalAt index: Int)
}

class FullscreenImageVC: UIViewController {
    
    weak var delegate: FullscreenImageVCDelegate?
    
    var searchContentType: ContentType = .image
    var content: ImageSearchResponse
    
    var currentIndex: Int = 0 {
        didSet {
            updateUI()
        }
    }
    
    private var cancellable: Set<AnyCancellable> = Set<AnyCancellable>()

    private var mainView: FullScreenImageView {
        return view as! FullScreenImageView
    }
    
    init() {
        self.content = ImageSearchResponse(imagesResults: [], videoResults: [], pagination: nil)
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        view = FullScreenImageView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTargets()
        updateUI()
    }
    
    private func setupTargets() {
        mainView.dismissButton.addTarget(self, action: #selector(closeView), for: .touchUpInside)
        mainView.nextButton.addTarget(self, action: #selector(nextButtonTapped), for: .touchUpInside)
        mainView.prevButton.addTarget(self, action: #selector(prevButtonTapped), for: .touchUpInside)
        mainView.openOriginalButton.addTarget(self, action: #selector(openOriginalButtonTapped), for: .touchUpInside)
    }
    
    private func updateUI() {
        var isImage = false
        var url: URL?
        
        switch searchContentType {
        case .image:
            if let imagesResults = content.imagesResults, imagesResults.indices.contains(currentIndex) {
                let imageUrlString = imagesResults[currentIndex].original
                if !imageUrlString.isEmpty {
                    url = URL(string: imageUrlString)
                    isImage = true
                    mainView.headerText.text = imagesResults[currentIndex].title ?? ""
                }
            }
            
        case .video:
            if let videoResults = content.videoResults, videoResults.indices.contains(currentIndex) {
                if let thumbnailUrlString = videoResults[currentIndex].thumbnail, !thumbnailUrlString.isEmpty {
                    url = URL(string: thumbnailUrlString)
                    mainView.headerText.text = videoResults[currentIndex].title ?? ""
                }
            }
        }
        
        if let url = url {
            loadImage(url)
        }
        
        updateButtonVisibility(isImage: isImage)
    }

    private func updateButtonVisibility(isImage: Bool) {
        mainView.prevButton.isHidden = currentIndex == 0
        mainView.nextButton.isHidden = isLastItem() 
    }

    private func isLastItem() -> Bool {
        switch searchContentType {
        case .image:
            if let imagesResults = content.imagesResults {
                return currentIndex == imagesResults.count - 1
            }
        case .video:
            if let videoResults = content.videoResults {
                return currentIndex == videoResults.count - 1
            }
        }
        return true
    }

    private func loadImage(_ url: URL?) {
        guard let url = url else { return }
        ImageLoader.setImage(url: url, imgView: mainView.imageView)
    }
    
    @objc private func closeView() {
        dismiss(animated: true)
    }
    
    @objc private func nextButtonTapped() {
        delegate?.fullscreenImageVC(self, didSelectNextAt: currentIndex)
    }
    
    @objc private func prevButtonTapped() {
        delegate?.fullscreenImageVC(self, didSelectPreviousAt: currentIndex)
    }
    
    @objc private func openOriginalButtonTapped() {
        delegate?.fullscreenImageVC(self, didTapOpenOriginalAt: currentIndex)
    }
}
