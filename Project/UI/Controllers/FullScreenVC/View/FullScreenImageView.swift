//
//  FullScreenImageView.swift
//  TestIos
//
//  Created by Антон Темеров on 24.06.2024.
//

import UIKit
import SnapKit

class FullScreenImageView: UIView {
    
    let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        
        return imageView
    }()
    
    let nextButton: PrimaryButton = {
        let button = PrimaryButton()
        button.setTitle("Next", for: .normal)
        
        return button
    }()

    let prevButton: PrimaryButton = {
        let button = PrimaryButton()
        button.setTitle("Previous", for: .normal)
        
        return button
    }()

    let openOriginalButton: PrimaryButton = {
        let button = PrimaryButton()
        button.setTitle("Original", for: .normal)
        
        return button
    }()

    
    let dismissButton: UIButton = {
        let dismissButton = UIButton()
        dismissButton.setImage(UIImage(systemName: "xmark"), for: .normal)
        dismissButton.tintColor = .black
        dismissButton.backgroundColor = Palette.Colors.primaryGray
        dismissButton.layer.cornerRadius = 10
        dismissButton.contentEdgeInsets = UIEdgeInsets(top: 10.0, left: 10.0, bottom: 10.0, right: 10.0)
        dismissButton.clipsToBounds = true
        
        return dismissButton
    }()
    
    let headerText: UILabel = {
        let headerText = UILabel()
        headerText.font = UIFont.boldSystemFont(ofSize: 16)
        headerText.textColor = .black
        headerText.textAlignment = .center
        headerText.numberOfLines = 0
        headerText.lineBreakMode = .byWordWrapping
        headerText.text = "Image"
        
        return headerText
    }()
    
    private let headerView: UIView = {
        let header = UIView()
        header.backgroundColor = Palette.Colors.primaryGray?.withAlphaComponent(0.4)
        
        return header
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        
        setupView()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        addSubview(headerView)
        headerView.addSubview(headerText)
        headerView.addSubview(dismissButton)
        addSubview(imageView)
        addSubview(nextButton)
        addSubview(prevButton)
        addSubview(openOriginalButton)
    }
    
    private func setupConstraints() {
        headerView.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.leading.trailing.equalToSuperview()
            make.height.equalToSuperview().multipliedBy(0.12)
        }
        
        headerText.snp.makeConstraints { make in
            make.center.equalTo(headerView.safeAreaLayoutGuide)
            make.leading.greaterThanOrEqualTo(headerView.safeAreaLayoutGuide.snp.leading).offset(60)
            make.trailing.lessThanOrEqualTo(headerView.safeAreaLayoutGuide.snp.trailing).offset(-60)
            make.bottom.equalToSuperview().inset(10)
        }
        
        dismissButton.snp.makeConstraints { make in
            make.centerY.equalTo(headerView.safeAreaLayoutGuide)
            make.trailing.equalToSuperview().inset(20)
            make.width.height.equalTo(30)
        }
        
        imageView.snp.makeConstraints { make in
            make.top.equalTo(headerView.snp.bottom).offset(20)
            make.leading.trailing.equalToSuperview()
            make.height.equalToSuperview().multipliedBy(0.8)
        }
        
        nextButton.snp.makeConstraints { make in
            make.trailing.equalToSuperview().inset(20)
            make.bottom.equalTo(safeAreaLayoutGuide).offset(-20)
        }
        
        prevButton.snp.makeConstraints { make in
            make.leading.equalToSuperview().inset(20)
            make.bottom.equalTo(safeAreaLayoutGuide).offset(-20)
        }
        
        openOriginalButton.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.bottom.equalTo(safeAreaLayoutGuide).offset(-20)
            make.leading.equalTo(prevButton.snp.trailing).offset(20)
            make.trailing.equalTo(nextButton.snp.leading).offset(-20)
        }

    }
}
