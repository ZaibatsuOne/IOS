//
//  WebViewVC.swift
//  TestIos
//
//  Created by Антон Темеров on 24.06.2024.
//

import WebKit
import SnapKit

class WebViewController: UIViewController {
    
    private(set) var webView: WKWebView!
    var onDismiss: (() -> Void)?
    
    private(set) var url: URL
    
    private(set) var headerView: UIView = {
        let view = UIView()
        view.backgroundColor = .lightGray.withAlphaComponent(0.3)
        return view
    }()
    
    private(set) var titleLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 16, weight: .semibold)
        label.textAlignment = .center
        return label
    }()
    
    private(set) var closeButton: UIButton = {
        let button = UIButton()
        button.setTitle("Close", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.addTarget(self, action: #selector(closeWebView), for: .touchUpInside)
        return button
    }()
    
    private let loaderView = LoaderView()
    
    init(url: URL) {
        self.url = url
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        setupWebView()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        onDismiss?()
    }
    
    private func setupUI() {
        view.addSubview(headerView)
        headerView.addSubview(titleLabel)
        headerView.addSubview(closeButton)
        
        headerView.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top)
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(60)
        }
        
        titleLabel.snp.makeConstraints { make in
            make.center.equalTo(headerView)
        }
        
        closeButton.snp.makeConstraints { make in
            make.centerY.equalTo(headerView)
            make.trailing.equalToSuperview().offset(-16)
        }
        
        view.addSubview(loaderView)
        loaderView.snp.makeConstraints { make in
            make.center.equalTo(view)
        }
        
        titleLabel.text = "Web View"
        
        view.backgroundColor = .white
    }
    
    private func setupWebView() {
        webView = WKWebView()
        webView.navigationDelegate = self
        webView.isHidden = true 
        view.addSubview(webView)
        
        webView.snp.makeConstraints { make in
            make.top.equalTo(headerView.snp.bottom)
            make.leading.trailing.bottom.equalToSuperview()
        }
        
        let request = URLRequest(url: url)
        webView.load(request)
    }
    
    @objc internal func closeWebView() {
        dismiss(animated: true, completion: nil)
    }
    
}

extension WebViewController: WKNavigationDelegate {

    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        loaderView.show()
        webView.isHidden = true
    }

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        loaderView.hide()
        loaderView.isHidden = true
        webView.isHidden = false
    }

    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        handleError(error)
    }

    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        handleError(error)
    }

    private func handleError(_ error: Error) {
        let nsError = error as NSError
        if nsError.code == NSURLErrorTimedOut {
            print("WebView failed to load: The request timed out.")
        } else {
            print("WebView failed to load with error: \(error.localizedDescription)")
        }
        showSimpleAlert(title: "Oops...", message: "This page doesn't seem to load...") {
            self.dismiss(animated: true, completion: nil)
        }
    }

}
