//
//  Palette.swift
//  TestIos
//
//  Created by Антон Темеров on 25.06.2024.
//

import UIKit

struct Palette {
    
    enum Colors {
        static let primaryGray = UIColor(named: "primaryGray")
        static let primaryBlue = UIColor(named: "primaryBlue")!
    }
    
}
