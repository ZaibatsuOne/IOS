//
//  FilterVC.swift
//  TestIos
//
//  Created by Антон Темеров on 24.06.2024.
//

import UIKit
import Combine

protocol FilterViewControllerDelegate: AnyObject {
    func filterViewController(_ viewController: FilterViewController, didApplyFilters filters: [String: Any])
}

class FilterViewController: UIViewController {
    
    weak var delegate: FilterViewControllerDelegate?
    
    var queryText: String?

    private var cancellables: Set<AnyCancellable> = []
    private(set) var filters: [String: Any] = [:]
    private var mainView: FilterView { return view as! FilterView }
        
    // List of content type with index
    private let contentType: [(type: ContentType, index: Int)] = [
        (.image, 0),
        (.video, 1)
    ]
    
    // List of img size with index
    private let imgSize: [(size: ImageSize, index: Int)] = [
        (.any, 0),
        (.large, 1),
        (.medium, 2),
        (.icon, 3)
    ]
    
    // List of languages with index
    private let languages: [(language: Language, index: Int)] = [
        (.english, 0),
        (.spanish, 1),
        (.french, 2)
    ]
    
    // List of countries with index
    private let countries: [(country: Countries, index: Int)] = [
        (.ru, 0),
        (.usa, 1),
        (.uk, 2)
    ]
    
    private(set) var selectedContentTypeSubject = CurrentValueSubject<ContentType, Never>(.image)
    private(set) var selectedSizeSubject = CurrentValueSubject<ImageSize, Never>(.any)
    private(set) var selectedCountrySubject = CurrentValueSubject<Countries, Never>(.ru)
    private(set) var selectedLanguageSubject = CurrentValueSubject<Language, Never>(.english)
    
    override func loadView() {
        view = FilterView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDelegates()
        setupTargets()
        setupBindings()
        
        // Print UserSettings for debugging
        printUserSettings()
    }
    
    private func setupDelegates() {
        mainView.tableView.delegate = self
        mainView.tableView.dataSource = self
    }
    
    private func setupTargets() {
        mainView.applyButton.addTarget(self, action: #selector(applyFilters), for: .touchUpInside)
    }
    
    private func setupBindings() {
        // Set initial values from UserSettings if available
        if let contentTypeString = UserSettings.contentType {
            selectedContentTypeSubject.value = ContentType(rawValue: contentTypeString) ?? .image
        }
        
        if let sizeString = UserSettings.size {
            selectedSizeSubject.value = ImageSize(rawValue: sizeString) ?? .any
        }
        
        if let countryString = UserSettings.country {
            selectedCountrySubject.value = Countries(rawValue: countryString) ?? .ru
        }
        
        if let languageString = UserSettings.language {
            selectedLanguageSubject.value = Language(rawValue: languageString) ?? .english
        }
        
        // Bindings for updating filters dictionary
        selectedContentTypeSubject
            .sink { [weak self] type in
                guard let self = self else { return }
                self.filters["type"] = type.rawValue
                // Update UI based on selected content type
                self.updateUIForContentType(type)
            }
            .store(in: &cancellables)
        
        selectedSizeSubject
            .sink { [weak self] size in
                guard let self = self else { return }
                self.filters["size"] = size.rawValue
            }
            .store(in: &cancellables)
        
        selectedCountrySubject
            .sink { [weak self] country in
                guard let self = self else { return }
                self.filters["country"] = country.rawValue
            }
            .store(in: &cancellables)
        
        selectedLanguageSubject
            .sink { [weak self] language in
                guard let self = self else { return }
                self.filters["language"] = language.rawValue
            }
            .store(in: &cancellables)
        
        queryText.publisher
            .sink { [weak self] text in
                guard let self = self else { return }
                self.updateApplyButtonState()
            }
            .store(in: &cancellables)
    }
    
    private func saveFiltersToUserDefaults() {
        // Save filters to UserDefaults
        UserSettings.contentType = selectedContentTypeSubject.value.rawValue
        UserSettings.size = selectedSizeSubject.value.rawValue
        UserSettings.country = selectedCountrySubject.value.rawValue
        UserSettings.language = selectedLanguageSubject.value.rawValue
    }
    
    @objc internal func applyFilters() {
        delegate?.filterViewController(self, didApplyFilters: filters)
        
        saveFiltersToUserDefaults()
        
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.dismiss(animated: true)
        }
    }
    
    private func printUserSettings() {
        print("=============")
        print("UserSettings:")
        print("ContentType: \(UserSettings.contentType ?? "nil")")
        print("Size: \(UserSettings.size ?? "nil")")
        print("Country: \(UserSettings.country ?? "nil")")
        print("Language: \(UserSettings.language ?? "nil")")
        print("=============")
    }
    
    private func updateUIForContentType(_ type: ContentType) {
        // Update the UI without hiding the country and language cells
        mainView.tableView.beginUpdates()
        if type == .image {
            mainView.tableView.insertRows(at: [IndexPath(row: 3, section: 0)], with: .fade)
        } else {
            mainView.tableView.deleteRows(at: [IndexPath(row: 3, section: 0)], with: .fade)
        }
        mainView.tableView.endUpdates()
    }
    
    private func updateApplyButtonState() {
        let isQueryTextEmpty = queryText?.isEmpty ?? true
        
        mainView.applyButton.isEnabled = !isQueryTextEmpty
        
        if isQueryTextEmpty {
            mainView.applyButton.backgroundColor = Palette.Colors.primaryGray
        } else {
            mainView.applyButton.backgroundColor = Palette.Colors.primaryBlue
        }
    }
}

extension FilterViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return selectedContentTypeSubject.value == .image ? 4 : 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: FilterTableViewCell.reuseIdentifier, for: indexPath) as! FilterTableViewCell
        
        switch indexPath.row {
        case 0:
            let selectedIndex = contentType.firstIndex { $0.type == selectedContentTypeSubject.value } ?? 0
            configureCell(cell, with: "Type", values: contentType.map { $0.type.displayName }, selectedIndex: selectedIndex)
        case 1:
            let selectedIndex = countries.firstIndex { $0.country == selectedCountrySubject.value } ?? 0
            configureCell(cell, with: "Countries", values: countries.map { $0.country.displayName }, selectedIndex: selectedIndex)
        case 2:
            let selectedIndex = languages.firstIndex { $0.language == selectedLanguageSubject.value } ?? 0
            configureCell(cell, with: "Language", values: languages.map { $0.language.displayName }, selectedIndex: selectedIndex)
        case 3:
            if selectedContentTypeSubject.value == .image {
                let selectedIndex = imgSize.firstIndex { $0.size == selectedSizeSubject.value } ?? 0
                configureCell(cell, with: "Size", values: imgSize.map { $0.size.displayName }, selectedIndex: selectedIndex)
            } else {
                cell.isHidden = true
            }
        default:
            break
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    // Configure each cell based on indexPath
    private func configureCell(_ cell: FilterTableViewCell, with title: String, values: [String], selectedIndex: Int) {
        cell.isHidden = false
        cell.titleLabel.text = title
        cell.segmentedControl.removeAllSegments()
        
        for (index, value) in values.enumerated() {
            cell.segmentedControl.insertSegment(withTitle: value, at: index, animated: false)
        }
        
        cell.segmentedControl.selectedSegmentIndex = selectedIndex
        
        cell.segmentedControl.addTarget(self, action: #selector(segmentedControlValueChanged(_:)), for: .valueChanged)
    }
    
    // Handle segmented control value changes
    @objc private func segmentedControlValueChanged(_ sender: UISegmentedControl) {
        let point = sender.convert(CGPoint.zero, to: mainView.tableView)
        
        guard let indexPath = mainView.tableView.indexPathForRow(at: point) else {
            return
        }
        
        switch indexPath.row {
        case 0:
            selectedContentTypeSubject.send(contentType[sender.selectedSegmentIndex].type)
        case 1:
            selectedCountrySubject.send(countries[sender.selectedSegmentIndex].country)
        case 2:
            selectedLanguageSubject.send(languages[sender.selectedSegmentIndex].language)
        case 3:
            selectedSizeSubject.send(imgSize[sender.selectedSegmentIndex].size)
        default:
            break
        }
    }
}
