//
//  MainVC.swift
//  TestIos
//
//  Created by Антон Темеров on 24.06.2024.
//

import UIKit
import Combine
import OSLog

class MainVC: UIViewController {
    
    private var provider: MainProvider = MainProvider()
    private var cancellables: Set<AnyCancellable> = []
    private let logger = Logger()
    
    // MARK: - Published Properties
    
    // Query params
    @Published private var currentPage: Int = 0
    @Published private var searchContentType: ContentType = .image
    @Published private var searchImgSize: ImageSize = .any
    @Published private var searchLang: Language = .english
    @Published private var searchCountry: Countries = .ru
    @Published private var searchText: String = ""
    
    // Response params
    @Published private var imagesByQuery: ImageSearchResponse = ImageSearchResponse(
        imagesResults: [],
        videoResults: [],
        pagination: nil
    )
    
    // MARK: - Private Properties
    
    private lazy var searchRequest: ImageSearchRequest = {
        return ImageSearchRequest(
            query: self.searchText,
            imageSize: self.searchImgSize.rawValue,
            contentType: self.searchContentType.rawValue,
            page: self.currentPage,
            language: self.searchLang.rawValue,
            country: self.searchCountry.rawValue
        )
    }()
    
    // MARK: - Views
    
    private lazy var fullscreenImageVC: FullscreenImageVC = {
        let vc = FullscreenImageVC()
        vc.delegate = self
        return vc
    }()
    
    private lazy var mainView: MainView = {
        return view as! MainView
    }()
    
    // MARK: - Lifecycle Methods
    
    override func loadView() {
        view = MainView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Search photo from Google"
        mainView.loaderView.hide()

        loadDefaultValuesFromUserSettings()
        setupBindings()
        setupDelegates()
        hideKeyboardOnBackgroundTouched()
    }
    
    // MARK: - Setup Methods
    
    private func setupBindings() {
        let params = Publishers.CombineLatest3($searchLang, $searchCountry, $currentPage)
        
        $searchText.combineLatest(params, $searchImgSize, $searchContentType)
            .dropFirst()
            .debounce(for: .seconds(1.0), scheduler: RunLoop.main)
            .sink { [weak self] text, params, imgSize, contentType in
                guard let self = self else { return }
                
                let (language, country, page) = params
                
                self.searchRequest.query = text
                self.searchRequest.page = page
                self.searchRequest.imageSize = imgSize.rawValue
                self.searchRequest.contentType = contentType.rawValue
                self.searchRequest.language = language.rawValue
                self.searchRequest.country = country.rawValue
                
                self.fetchImagesByQuery(self.searchRequest)
            }
            .store(in: &cancellables)
        
        $imagesByQuery
            .sink { [weak self] images in
                guard let self = self else { return }
                if images.imagesResults?.count ?? 0 > 0 || images.videoResults?.count ?? 0 > 0 {
                    self.setupFilterButtonIfNeeded()
                }
            }
            .store(in: &cancellables)
    }
    
    private func setupDelegates() {
        mainView.searchBar.delegate = self
        mainView.collectionView.delegate = self
        mainView.collectionView.dataSource = self
    }
    
    private func setupFilterButtonIfNeeded() {
        if self.navigationItem.rightBarButtonItem == nil {
            let filterButton = UIBarButtonItem(
                image: UIImage(systemName: "gearshape.fill")?.withRenderingMode(.alwaysTemplate),
                style: .plain,
                target: self,
                action: #selector(self.openFilters)
            )
            filterButton.tintColor = Palette.Colors.primaryGray
            self.navigationItem.rightBarButtonItem = filterButton
        }
    }
    
    private func showLoader() {
        mainView.loaderView.show()
        mainView.greetingLabel.isHidden = true
        mainView.collectionView.isUserInteractionEnabled = false
        mainView.collectionView.alpha = 0.3
        self.navigationItem.rightBarButtonItem?.isEnabled = false
    }
    
    private func hideLoader() {
        mainView.loaderView.hide()
        mainView.collectionView.isUserInteractionEnabled = true
        mainView.collectionView.alpha = 1
        self.navigationItem.rightBarButtonItem?.isEnabled = true
    }
    
    // MARK: - Action Methods
    
    @objc private func openFilters() {
        let filterVC = FilterViewController()
        filterVC.delegate = self
        filterVC.queryText = searchText
        filterVC.modalPresentationStyle = .custom
        filterVC.transitioningDelegate = self
        
        present(filterVC, animated: true)
    }
    
    // Get response from API
    private func fetchImagesByQuery(_ request: ImageSearchRequest) {
        showLoader()
        provider.searchPhotoByQuery(request)
            .sink(
                receiveCompletion: { [weak self] completion in
                    guard let self = self else { return }
                    self.hideLoader()
                    switch completion {
                    case .finished: break
                    case .failure(let error):
                        self.logger.error("API Error: \(error.localizedDescription)")
                    }
                },
                receiveValue: { [weak self] response in
                    guard let self = self else { return }
                    self.handleResponse(response, request: request)
                }
            )
            .store(in: &cancellables)
    }
    
    // Configure response from API
    private func handleResponse(_ response: ImageSearchResponse, request: ImageSearchRequest) {
        switch searchContentType {
        case .image:
            handleImageResponse(response, request: request)
        case .video:
            handleVideoResponse(response, request: request)
        }
    }

    private func handleImageResponse(_ response: ImageSearchResponse, request: ImageSearchRequest) {
        guard let imagesResults = response.imagesResults else {
            handleNoResults()
            return
        }
        
        if request.page == 0 {
            imagesByQuery = response
        } else {
            imagesByQuery.imagesResults?.append(contentsOf: imagesResults)
            imagesByQuery.pagination = response.pagination
        }
        
        reloadCollectionView()
    }

    private func handleVideoResponse(_ response: ImageSearchResponse, request: ImageSearchRequest) {
        guard let videoResults = response.videoResults else {
            handleNoResults()
            return
        }
        
        if request.page == 0 {
            imagesByQuery = response
        } else {
            imagesByQuery.videoResults?.append(contentsOf: videoResults)
            imagesByQuery.pagination = response.pagination
        }
        
        reloadCollectionView()
    }
    
    private func reloadCollectionView() {
        mainView.collectionView.reloadData()
    }
    
    private func handleNoResults() {
        let contentTypeString = (searchContentType == .image) ? "image" : "video"
        logger.info("No \(contentTypeString) search results found")
        showSimpleAlert(title: "Error", message: "No \(contentTypeString) search results found")
    }
    
    // MARK: - UserSettings Loading

    private func loadDefaultValuesFromUserSettings() {
        if let contentTypeString = UserSettings.contentType {
            searchContentType = ContentType(rawValue: contentTypeString) ?? .image
        }
        
        if let sizeString = UserSettings.size {
            searchImgSize = ImageSize(rawValue: sizeString) ?? .any
        }
        
        if let countryString = UserSettings.country {
            searchCountry = Countries(rawValue: countryString) ?? .ru
        }
        
        if let languageString = UserSettings.language {
            searchLang = Language(rawValue: languageString) ?? .english
        }
    }
    
    deinit {
        cancellables.forEach { $0.cancel() }
    }
}

// MARK: - UISearchBarDelegate

extension MainVC: UISearchBarDelegate {
    
    // Check empty value in search text
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard !searchText.trimmingCharacters(in: .whitespaces).isEmpty else { return }
        self.searchText = searchText
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}

// MARK: - UICollectionViewDelegate, UICollectionViewDataSource

extension MainVC: UICollectionViewDelegate, UICollectionViewDataSource {
    
    // Count of items in collections
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        var resultsCount = 0
        
        switch searchContentType {
        case .image:
            if let imageResults = imagesByQuery.imagesResults {
                resultsCount = imageResults.count
            }
        case .video:
            if let videoResults = imagesByQuery.videoResults {
                resultsCount = videoResults.count
            }
        }
        
        return resultsCount
    }
    
    // Configure cell in collection
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MainViewImageCell.reuseIdentifier, for: indexPath) as! MainViewImageCell
        
        switch searchContentType {
        case .image:
            guard let imageResult = imagesByQuery.imagesResults?[indexPath.row], let imageUrl = URL(string: imageResult.thumbnail) else {
                return cell
            }
            cell.configure(imageUrl)
            
        case .video:
            guard let videoResult = imagesByQuery.videoResults?[indexPath.row], let thumbnailUrl = URL(string: videoResult.thumbnail ?? "") else {
                return cell
            }
            cell.configure(thumbnailUrl)
        }
        
        return cell
    }
    
    // Open full screen image carousel
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch searchContentType {
        case .image:
            guard let imageResults = imagesByQuery.imagesResults as? [ImageResult] else { return }
        case .video:
            guard let videoResults = imagesByQuery.videoResults as? [VideoResult] else { return }
        }
        
        fullscreenImageVC.searchContentType = searchContentType
        fullscreenImageVC.content = imagesByQuery
        fullscreenImageVC.currentIndex = indexPath.item
        fullscreenImageVC.modalPresentationStyle = .fullScreen
        present(fullscreenImageVC, animated: true, completion: nil)
    }

    
    // Handle pagination
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if (imagesByQuery.pagination?.next) != nil {
            switch searchContentType {
            case .image:
                guard let imageResults = imagesByQuery.imagesResults, !imageResults.isEmpty else { return }
                if indexPath.row == imageResults.count - 1 {
                    currentPage += 1
                }
            case .video:
                guard let videoResults = imagesByQuery.videoResults, !videoResults.isEmpty else { return }
                if indexPath.row == videoResults.count - 1 {
                    currentPage += 10
                }
            }
        }
    }

}


// MARK: - FullscreenImageVCDelegate

extension MainVC: FullscreenImageVCDelegate {
    
    // Next button logic
    func fullscreenImageVC(_ viewController: FullscreenImageVC, didSelectNextAt index: Int) {
        var resultsCount = 0
        
        switch searchContentType {
        case .image:
            if let results = imagesByQuery.imagesResults {
                resultsCount = results.count
            }
        case .video:
            if let results = imagesByQuery.videoResults {
                resultsCount = results.count
            }
        }
        
        if index >= 0 && index < resultsCount - 1 {
            viewController.currentIndex = index + 1
        }
    }
    
    
    // Previous button logic
    func fullscreenImageVC(_ viewController: FullscreenImageVC, didSelectPreviousAt index: Int) {
        guard index > 0 else { return }
        viewController.currentIndex = index - 1
    }
    
    // Open original image in web view logic
    func fullscreenImageVC(_ viewController: FullscreenImageVC, didTapOpenOriginalAt index: Int) {
        var url: URL?
        
        switch searchContentType {
        case .image:
            guard let imageResults = imagesByQuery.imagesResults as? [ImageResult], index < imageResults.count else { return }
            guard let urlString = imageResults[index].link, let imageURL = URL(string: urlString) else { return }
            url = imageURL
            
        case .video:
            guard let videoResults = imagesByQuery.videoResults as? [VideoResult], index < videoResults.count else { return }
            guard let urlString = videoResults[index].link, let videoURL = URL(string: urlString) else { return }
            url = videoURL
        }
        
        guard let finalURL = url else { return }

        let currentIndex = viewController.currentIndex
        let currentContent = viewController.content
        
        viewController.dismiss(animated: true) { [weak self] in
            guard let self = self else { return }
            let webVC = WebViewController(url: finalURL)
            present(webVC, animated: true)
            
            // Reopen FullscreenImageVC with previous state
            webVC.onDismiss = {
                viewController.content = currentContent
                viewController.currentIndex = currentIndex
                self.present(viewController, animated: true, completion: nil)
            }
        }
    }
    
}


// MARK: - FilterViewControllerDelegate
extension MainVC: FilterViewControllerDelegate {
    func filterViewController(_ viewController: FilterViewController, didApplyFilters filters: [String: Any]) {
        currentPage = 0
        
        DispatchQueue.main.async {
            if let indexPath = self.mainView.collectionView.indexPathsForVisibleItems.sorted().first {
                self.mainView.collectionView.scrollToItem(at: indexPath, at: .top, animated: true)
            }
        }

        if let contentType = filters["type"] as? String,
           let type = ContentType(rawValue: contentType) {
            searchContentType = type
        }   
        
        if let imageSize = filters["size"] as? String,
           let size = ImageSize(rawValue: imageSize) {
            print("imageSize: \(imageSize)")
            searchImgSize = size
        }
        
        
        if let languageCode = filters["language"] as? String,
           let language = Language(rawValue: languageCode) {
            print("languageCode: \(languageCode)")
            searchLang = language
        }
        
        if let countryCode = filters["country"] as? String,
           let country = Countries(rawValue: countryCode) {
            searchCountry = country
        }
    }
}

// MARK: - HalfSvreenDelegate
extension MainVC: UIViewControllerTransitioningDelegate {
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        return HalfSizePresentationController(presentedViewController: presented, presenting: presenting)
    }
}
