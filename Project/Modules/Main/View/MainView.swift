//
//  MainView.swift
//  TestIos
//
//  Created by Антон Темеров on 24.06.2024.
//

import UIKit
import SnapKit

class MainView: UIView {
    
    let loaderView: LoaderView = {
        let loader = LoaderView()
        
        return loader
    }()
    
    var searchBar: UISearchBar = {
        let searchBar = UISearchBar()
        searchBar.placeholder = "Search for images"
        searchBar.searchBarStyle = .minimal
        searchBar.barTintColor = .white
        
        return searchBar
    }()
    
    var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: 100, height: 100)
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = .white
        collectionView.register(MainViewImageCell.self, forCellWithReuseIdentifier: MainViewImageCell.reuseIdentifier)
        
        return collectionView
    }()
    
    let greetingLabel: UILabel = {
        let greetingLabel = UILabel()
        greetingLabel.text = "To start, enter the value in the search bar"
        greetingLabel.textAlignment = .center
        greetingLabel.textColor = .gray.withAlphaComponent(0.5)
        
        return greetingLabel
    }()
    
    private let contentView: UIView = {
        let contentView = UIView()
        
        return contentView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        backgroundColor = .white
        
        addSubview(contentView)
        
        contentView.addSubview(searchBar)
        contentView.addSubview(collectionView)
        contentView.addSubview(greetingLabel)
        contentView.addSubview(loaderView)
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        contentView.snp.makeConstraints { make in
            make.top.equalTo(safeAreaLayoutGuide.snp.top)
            make.leading.equalTo(safeAreaLayoutGuide.snp.leading)
            make.trailing.equalTo(safeAreaLayoutGuide.snp.trailing)
            make.bottom.equalTo(safeAreaLayoutGuide.snp.bottom)
        }
        
        searchBar.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(20)
            make.leading.trailing.equalToSuperview().inset(20)
            make.height.equalTo(40)
        }
        
        greetingLabel.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.leading.trailing.equalToSuperview().inset(30)
        }
        
        collectionView.snp.makeConstraints { make in
            make.top.equalTo(searchBar.snp.bottom).offset(20)
            make.leading.equalToSuperview().inset(20)
            make.trailing.equalToSuperview().inset(20)
            make.bottom.equalToSuperview().offset(-60)
        }
        
        loaderView.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
    }
 

}
