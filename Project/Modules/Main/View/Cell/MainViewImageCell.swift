//
//  MainViewImageCell.swift
//  TestIos
//
//  Created by Антон Темеров on 24.06.2024.
//

import UIKit
import SnapKit

class MainViewImageCell: UICollectionViewCell {
    
    static let reuseIdentifier = "MainViewImgCell"
    
    var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(_ url: URL) {
        ImageLoader.setImage(url: url, imgView: imageView)
    }
    
    private func setupView() {
        addSubview(imageView)
    }
    
    private func setupConstraints() {
        imageView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
    
}
