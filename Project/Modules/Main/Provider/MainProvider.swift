//
//  MainProvider.swift
//  TestIos
//
//  Created by Антон Темеров on 24.06.2024.
//

import Moya
import Combine

protocol MainProviderProtocol {
    func searchPhotoByQuery(_ request: ImageSearchRequest) -> AnyPublisher<ImageSearchResponse, MoyaError>
}

class MainProvider: MainProviderProtocol {
    
    private let provider: MoyaProvider<ImageSearchAPI> = MoyaProvider<ImageSearchAPI>()
    
    func searchPhotoByQuery(_ request: ImageSearchRequest) -> AnyPublisher<ImageSearchResponse, MoyaError> {
        provider.requestPublisher(.searchImages(request: request))
            .map(ImageSearchResponse.self)
            .subscribe(on: DispatchQueue.global(qos: .userInteractive))
            .receive(on: DispatchQueue.main)
            .eraseToAnyPublisher()
    }
    
}
