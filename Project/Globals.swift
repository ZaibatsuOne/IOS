//
//  Globals.swift
//  TestIos
//
//  Created by Антон Темеров on 24.06.2024.
//

import Foundation

struct Globals {
    
    static var API_KEY: String {
        guard let path = Bundle.main.path(forResource: "Config", ofType: "plist"),
              let configDict = NSDictionary(contentsOfFile: path),
              let apiKey = configDict["API_KEY"] as? String else {
            fatalError("API_KEY not found in Config.plist")
        }
        return apiKey
    }
    
}
